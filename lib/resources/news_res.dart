import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

class NewsRes {
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;
  final FirebaseAuth _auth = FirebaseAuth.instance;

  Future<String> likeNews(String newsId) async {
    String res = 'Some error occured';
    try {
      await _firestore.collection('news').doc(newsId).get().then((value) {
        if(value.data()!['like'].contains(_auth.currentUser!.uid)){
          _firestore
            .collection('news')
            .doc(newsId).update({"like": FieldValue.arrayRemove([_auth.currentUser!.uid])});
        }else{
          _firestore
            .collection('news')
            .doc(newsId).update({"like": [_auth.currentUser!.uid]});
        }
      });
      // for(var cek in result.get('like')){
      //   print.call(cek);
      //   if(cek == _auth.currentUser!.uid){
      //
      //   }
      // }
      // await _firestore
      //     .collection('news')
      //     .doc(newsId).update({"like": [_auth.currentUser!.uid]});

      res = 'success';
    } catch (error) {
      res = error.toString();
    }
    return res;
  }

  Future<String> sendComment(String comment, String newsId) async {
    String res = 'Some error occured';
    try {
      await _firestore
          .collection('news')
          .doc(newsId)
          .collection('komentar')
          .add({
        'komentar': comment,
        'send_by': _auth.currentUser!.uid,
        'created_time': DateTime.now(),
      });

      res = 'success';
    } catch (error) {
      res = error.toString();
    }
    return res;
  }

  Future deleteMessage() async {}


}