import 'package:flutter/animation.dart';

class ThemeColors {
  Color blue = const Color(0XFF28499D);
  Color yellow = Color.fromARGB(255, 255, 229, 164);
  Color pink = Color.fromARGB(255, 247, 120, 186);
  Color grey = Color.fromARGB(255, 235, 228, 255);
  Color purple = Color.fromARGB(255, 175, 148, 235);
  Color purpleAccent = Color.fromARGB(255, 197, 175, 231);
}
