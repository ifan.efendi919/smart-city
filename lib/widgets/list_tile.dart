// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors

import 'package:badges/badges.dart' as badges;
import 'package:flutter/material.dart';

import '../theme/theme_colors.dart';

class ListTileWidget extends StatelessWidget {
  final String title;
  final String lastMessage;

  final dynamic? onTap;
  const ListTileWidget({
    super.key,
    required this.title,
    required this.lastMessage,
    this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 15),
      child: ListTile(
        onTap: onTap,
        leading: Container(
          height: 50,
          width: 50,
          decoration: BoxDecoration(
            color: ThemeColors().blue,
            borderRadius: BorderRadius.circular(100),
          ),
          child: Icon(
            Icons.group,
            color: Colors.white,
          ),
        ),
        title: Text(title),
        subtitle: Text(lastMessage),
      ),
    );
  }
}
