// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:smart_city/ui/home/group_chat.dart';
import 'package:smart_city/ui/home/scanqr.dart';

import '../../theme/theme_colors.dart';
import 'home.dart';
import 'projects_list.dart';
import 'settings.dart';

class MainHome extends StatefulWidget {
  const MainHome({super.key});

  @override
  State<MainHome> createState() => _MainHomeState();
}

class _MainHomeState extends State<MainHome> {
  int currentPage = 0;
  final List _pages = [
    HomePage(),
    // ProjectsList(),
    GroupChatPage(),
    // SettingsPage(),
  ];
  void tappedPage(int index) {
    setState(() {
      currentPage = index;
    });
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: _pages[currentPage],
      floatingActionButton: FloatingActionButton(
        onPressed: (){
          Get.to(() => ScanQrCode());
        },
        backgroundColor: ThemeColors().blue,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(100)
        ),
        isExtended: true,
        child: const Icon(
          Icons.qr_code_scanner_rounded,
          color: Colors.white,
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.miniCenterDocked,
      bottomNavigationBar: BottomNavigationBar(
        showSelectedLabels: false,
        showUnselectedLabels: false,
        backgroundColor: ThemeColors().blue,
        currentIndex: currentPage,
        type: BottomNavigationBarType.fixed,
        onTap: tappedPage,
        items: [
          BottomNavigationBarItem(
            icon: Icon(
              Icons.home_filled,
              color: Colors.white,
            ),
            activeIcon: Icon(
              Icons.home_filled,
              color: Colors.white,
            ),
            label: 'H',
          ),

          BottomNavigationBarItem(
            icon:Icon(
              Icons.groups,
              color: Colors.white,
            ),
            activeIcon: Icon(
              Icons.groups,
              color: Colors.white,
            ),
            label: 'S',
          ),
        ],
      ),
    );
  }
}
