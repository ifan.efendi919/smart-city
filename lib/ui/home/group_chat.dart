import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intl/intl.dart';
import 'package:smart_city/ui/home/detail_group.dart';

import '../../theme/theme_colors.dart';
import '../../widgets/list_tile.dart';
import '../project_ui/projects_chat_page.dart';

class GroupChatPage extends StatefulWidget {
  const GroupChatPage({Key? key}) : super(key: key);

  @override
  State<GroupChatPage> createState() => _GroupChatPageState();
}

class _GroupChatPageState extends State<GroupChatPage> {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        scrolledUnderElevation: 0,
        toolbarHeight: 15,
      ),
      body: Container(
        color: Colors.white,
        //Main column layout
        child: Column(
          children: [
            //header title section.
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Groups Chat',
                    style: TextStyle(
                      fontSize: 30,
                      fontWeight: FontWeight.bold,
                      color: ThemeColors().blue
                    ),
                  ),
                ],
              ),
            ),

            //New messages section.
            Expanded(
              child: SingleChildScrollView(
                physics: const BouncingScrollPhysics(),
                child: Column(
                  children: [
                    //Projects Section.
                    Padding(
                      padding:
                      const EdgeInsets.only(left: 15, bottom: 15, top: 10),
                      child: Row(
                        children: [
                          Text(
                            'Course',
                            style: TextStyle(
                              color: ThemeColors().blue,
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          const SizedBox(
                            width: 10,
                          ),
                          Container(
                            height: 25,
                            width: 30,
                            decoration: BoxDecoration(
                              color: ThemeColors().grey.withOpacity(0.5),
                              borderRadius: BorderRadius.circular(10),
                            ),
                            child: StreamBuilder(
                              stream: FirebaseFirestore.instance
                                  .collection('course')
                                  .where('member', arrayContains: FirebaseAuth.instance.currentUser!.uid)
                                  .snapshots(),
                              builder: (context, snapshot) {
                                if (!snapshot.hasData) {
                                  return Center(
                                    child: Text(
                                      '0',
                                      style: TextStyle(
                                        color: ThemeColors().pink,
                                        fontSize: 14,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  );
                                } else {
                                  return Center(
                                    child: Text(
                                      snapshot.data!.docs.length.toString(),
                                      style: TextStyle(
                                        color: ThemeColors().pink,
                                        fontSize: 14,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  );
                                }
                              },
                            ),
                          ),
                        ],
                      ),
                    ),

                    //Projects Lists.
                    StreamBuilder(
                      stream: FirebaseFirestore.instance
                        .collection('course')
                        .where('member', arrayContains: FirebaseAuth.instance.currentUser!.uid)
                        .snapshots(),
                      builder: ((context, snapshot) {
                        if (!snapshot.hasData) {
                          return Container();
                        } else {
                          return ListView.builder(
                            itemCount: snapshot.data!.docs.length,
                            physics: const ClampingScrollPhysics(),
                            shrinkWrap: true,
                            itemBuilder: ((context, index) {
                              var description = snapshot
                                  .data!.docs[index]['description']
                                  .toString();
                              var a = DateTime.parse(snapshot
                                  .data!.docs[index]['created_date']
                                  .toDate()
                                  .toString());
                              var time = DateFormat('d MMM y').format(a);
                              return ListTileWidget(
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => DetailGroupPage(
                                        projectName: snapshot.data!.docs[index]
                                        ['group_name'],
                                        projectId: snapshot.data!.docs[index].id,
                                        createDate: time,
                                      ),
                                    ),
                                  );
                                },
                                lastMessage: description,
                                title: snapshot.data!.docs[index]['group_name'],
                              );
                            }),
                          );
                        }
                      }),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
