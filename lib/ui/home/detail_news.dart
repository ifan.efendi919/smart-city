import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:smart_city/resources/news_res.dart';
import 'package:smart_city/theme/theme_colors.dart';
import 'package:timeago/timeago.dart' as timeago;

class DetailNews extends StatefulWidget {
  const DetailNews({Key? key, required this.id}) : super(key: key);
  final String id;

  @override
  State<DetailNews> createState() => _DetailNewsState();
}

class _DetailNewsState extends State<DetailNews> {
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;
  final TextEditingController _comment = TextEditingController();

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Detail',
          style: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.bold,
            color: Colors.white
          ),
        ),
        leading: IconButton(
          onPressed: (){
            Get.back();
          },
          icon: const Icon(Icons.arrow_back_ios_new),
          color: Colors.white,
        ),
        backgroundColor: ThemeColors().blue,
      ),
      body: ListView(
        children: [
          StreamBuilder(
            stream: _firestore.collection('news').doc(widget.id).snapshots(),
            builder: (context, snapshot){
              if(snapshot.connectionState == ConnectionState.waiting){
                return SpinKitFadingCircle(color: ThemeColors().blue,);
              }else if (snapshot.connectionState == ConnectionState.active
                  || snapshot.connectionState == ConnectionState.done) {
                if (snapshot.hasError) {
                  return const Text('Error');
                } else if (snapshot.hasData) {
                  return Container(
                    padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                    child: Column(
                      children: [
                        Text(
                          snapshot.data!.get('judul'),
                          style: const TextStyle(
                            fontSize: 16,
                            color: Colors.black,
                            fontWeight: FontWeight.bold
                          ),
                        ),
                        const SizedBox(height: 20.0,),
                        Html(data: snapshot.data!.get('isi_berita'),),
                        const SizedBox(height: 10,),
                        StreamBuilder(
                          stream: FirebaseFirestore.instance.collection('news').doc(widget.id).collection('komentar').orderBy('created_time', descending: false).snapshots(),
                          builder: (context, snap){
                            if(snap.connectionState == ConnectionState.waiting){
                              return SpinKitFadingCircle(color: ThemeColors().blue,);
                            } else if(snap.connectionState == ConnectionState.active
                                || snap.connectionState == ConnectionState.done){
                              if (snap.hasError) {
                                return const Text('Error');
                              }else if(snap.hasData){
                                return Column(
                                  children: [
                                    Container(
                                      alignment: Alignment.centerRight,
                                      child: Text('${snap.data!.docs.length} Komentar'),
                                    ),
                                    const Divider(),
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        GestureDetector(
                                          onTap: (){
                                            NewsRes().likeNews(snapshot.data!.id);
                                          },
                                          child: Row(
                                            children: [
                                              Icon(
                                                Icons.thumb_up_alt_outlined,
                                                color: snapshot.data!.data()!['like'].contains(FirebaseAuth.instance.currentUser!.uid)
                                                  ? ThemeColors().blue
                                                  : Colors.black,
                                              ),
                                              const SizedBox(width: 5,),
                                              Text(
                                                'Suka',
                                                style: TextStyle(
                                                  color: snapshot.data!.data()!['like'].contains(FirebaseAuth.instance.currentUser!.uid)
                                                    ? ThemeColors().blue
                                                    : Colors.black,
                                                )
                                              ),
                                            ],
                                          ),
                                        ),
                                        Row(
                                          children: const [
                                            Icon(Icons.chat_outlined),
                                            SizedBox(width: 5,),
                                            Text('Komentar'),
                                          ],
                                        ),
                                        Row(
                                          children: const [
                                            Icon(Icons.share),
                                            SizedBox(width: 5,),
                                            Text('Bagikan'),
                                          ],
                                        ),
                                      ],
                                    ),
                                    const Divider(),
                                    ListView(
                                      physics: const NeverScrollableScrollPhysics(),
                                      shrinkWrap: true,
                                      children: snap.data!.docs.map(
                                        (val) {
                                          return Column(
                                            children: [
                                              StreamBuilder(
                                                stream: _firestore.collection('users').doc(val.get('send_by')).snapshots(),
                                                builder: (context, user) {
                                                  if(user.connectionState == ConnectionState.waiting){
                                                    return SpinKitFadingCircle(color: ThemeColors().blue,);
                                                  }else if (user.connectionState == ConnectionState.active
                                                      || user.connectionState == ConnectionState.done) {
                                                    if (user.hasError) {
                                                      return const Text('Error');
                                                    } else if (user.hasData) {
                                                      DateTime date = DateTime.parse(val.get('created_time').toDate().toString());
                                                      final timeAgo = DateTime.now().subtract(Duration(minutes: DateTime.now().difference(date).inMinutes));
                                                      return Row(
                                                        crossAxisAlignment: CrossAxisAlignment.start,
                                                        children: [
                                                          Container(
                                                            height: 50,
                                                            width: 50,
                                                            decoration: BoxDecoration(
                                                              shape: BoxShape.circle,
                                                              image: DecorationImage(
                                                                image: NetworkImage(user.data!.get('photoUrl'))
                                                              )
                                                            ),
                                                          ),
                                                          const SizedBox(width: 5,),
                                                          Expanded(
                                                            flex: 1,
                                                            child: Column(
                                                              crossAxisAlignment: CrossAxisAlignment.start,
                                                              children: [
                                                                Text(user.data!.get('name')),
                                                                Text(
                                                                  val.get('komentar'),
                                                                ),
                                                                Text(timeago.format(timeAgo)),
                                                              ],
                                                            ),
                                                          ),
                                                        ],
                                                      );
                                                    } else {
                                                      return const Text('Empty data');
                                                    }
                                                  } else {
                                                    return Text('State: ${user.connectionState}');
                                                  }


                                                }
                                              ),
                                              const SizedBox(height: 10,)
                                            ],
                                          );
                                        }
                                      ).toList(),
                                    ),
                                  ],
                                );
                              } else {
                                return const Text('Empty data');
                              }
                            } else {
                              return Text('State: ${snap.connectionState}');
                            }
                          }
                        ),
                        const SizedBox(height: 20,),
                        Container(
                          padding: const EdgeInsets.symmetric(horizontal: 15),
                          decoration: BoxDecoration(
                            color: Colors.black12,
                            borderRadius: BorderRadius.circular(8)
                          ),
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Expanded(
                                flex: 1,
                                child: TextField(
                                  controller: _comment,
                                  decoration: const InputDecoration(
                                    hintText: 'Tulis komentar',
                                    border: InputBorder.none,
                                  ),
                                  // keyboardType: TextInputType.multiline,
                                  maxLines: 1,
                                  // inputFormatters: [BlacklistingTextInputFormatter(pattern)],
                                ),
                              ),
                              IconButton(
                                onPressed: (){
                                  NewsRes().sendComment(_comment.text, snapshot.data!.id);
                                  _comment.clear();
                                },
                                icon: const Icon(Icons.send)
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  );
                } else {
                  return const Text('Empty data');
                }
              } else {
                return Text('State: ${snapshot.connectionState}');
              }
            }
          ),
        ],
      )
      // StreamBuilder(
      //   stream: FirebaseFirestore.instance.collection('news').snapshots(),
      //   builder: (context, snapshot){
      //     if(snapshot.connectionState == ConnectionState.waiting){
      //       return const SpinKitFadingCircle(color: Colors.blue,);
      //     }else if (snapshot.connectionState == ConnectionState.active
      //         || snapshot.connectionState == ConnectionState.done) {
      //       if (snapshot.hasError) {
      //         return const Text('Error');
      //       } else if (snapshot.hasData) {
      //         return Text(snapshot.data!.docs[0].get('judul').toString());
      //       } else {
      //         return const Text('Empty data');
      //       }
      //     } else {
      //       return Text('State: ${snapshot.connectionState}');
      //     }
      //   },
      // ),
    );
  }
}
