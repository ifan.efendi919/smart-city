
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:get/get.dart';

import '../../theme/theme_colors.dart';

class FAQPAGE extends StatefulWidget {
  const FAQPAGE({Key? key}) : super(key: key);

  @override
  State<FAQPAGE> createState() => _FAQPAGEState();
}

class _FAQPAGEState extends State<FAQPAGE> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        scrolledUnderElevation: 0,
        backgroundColor: ThemeColors().blue,
        title: Row(
          children: const [
            Text('FAQ',
              style: TextStyle(
                fontSize: 18,
                color: Colors.white,
              ),
            ),
          ],
        ),
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back_ios_new_rounded,
            color: Colors.white,
          ),
          onPressed: (){
            Get.back();
          },
        ),
      ),
      body: ListView(
        shrinkWrap: true,
        padding: const EdgeInsets.all(20),
        children: [
          ExpansionTile(
            title: Text('Tata cara pembayaran'),
            // subtitle: Text('Trailing expansion arrow icon'),
            children: <Widget>[
              Html(data: '<p>Terdapat 2 metode pembayaran uang kuliah di universitas Widyatama, yaitu:</p><p>Metode pembayaran langsung</p><ul><li>Metode pembayaran langsung bisa dilakukan di Gedung B Lt.1 Ruang B124. Perlu diperhatikan pembayaran tidak bisa dilakukan dengan uang cash melainkan pembayaran non tunai dengan kartu debit maupun kredit</li></ul><p>Metode pembayaran transfer</p><ul><li>Cara Pembayaran Akun Virtual Bank OCBC Melalui OCBC Mobile</li></ul><ol><li>Pilih &ldquo;Payment &amp; Purchase&rdquo;</li><li>Pilih &ldquo;Other Payment&rdquo;</li><li>Pilih category &ldquo;Widyatama&rdquo; dan masukkan Customer ID</li><li>Contoh Customer ID: 106181030040</li><li>keterangan Customer ID: 1 adalah kode transaksi (1 untuk Reguler A dan 2 untuk Reguler B)</li><li>061810300400 adalah npm/no va</li><li>Pastikan untuk memeriksa tagihan, jika sudah sesuai lalu klik &ldquo;Continue&rdquo;</li><li>Lanjutkan proses otentikasi</li><li>Pembayaran selesai.</li></ol><ul><li>Cara Pembayaran Akun Virtual Bank OCBC Melalui Bank Transfer dari BCA</li></ul><ol><li>Pilih &ldquo;m-Transfer&rdquo;</li><li>Pilih &ldquo;Transfer antar Bank&rdquo;</li><li>Pilih Bank OCBCNISP</li><li>Pilih Rekening yang akan dituju, jika belum ada silahkan daftarkan terlebih dahulu</li><li>Contoh No rek tujuan : 90375106181030040</li><li>Keterangan No rek tujuan : 90375 adalah 5 digit kode bank</li><li>1 adalah kode transaksi (1 untuk Reguler A dan 2 untuk Reguler B)</li><li>06181030040 adalah npm/no va</li><li>Masukkan jumlah nominal sesuai tagihan, TANPA BIAYA ADMIN</li><li>Untuk Layanan transfer, disarankan memilih Realtime Online</li><li>Klik &ldquo;send&rdquo;</li><li>Lanjutkan proses otentikasi</li><li>Pembayaran Selesai.</li></ol><ul><li>Cara Pembayaran Akun Virtual Bank Niaga Melalui Bank Transfer dari BCA</li></ul><ol><li>Pilih &ldquo;Transfer&rdquo;</li><li>Pilih &ldquo;Transfer ke Bank Lain&rdquo;</li><li>Masukkan Bank tujuan &ldquo;CIMB Niaga&rdquo;</li><li>Pilih Rekening yang akan dituju, jika belum ada silahkan daftarkan terlebih dahulu</li><li>Contoh No rek tujuan : 243906181030040</li><li>Keterangan No rek tujuan : 2439 adalah 4 digit kode bank, 06181030040 adalah npm/no va</li><li>Masukkan Nominal sesuai tagihan, TANPA BIAYA ADMIN</li><li>Info nominal : nominal yang dimasukkan adalah nominal dari keseluruhan tagihan, tidak bisa memilih salah satu tagihan. Contoh, Riska anjani memiliki tagihan biaya kuliah sebesar Rp1.500.000 dan tagihan UTS susulan Rp500.000, maka nominal yang dimasukkan adalah Rp2.000.000</li><li>Untuk Layanan transfer, disarankan memilih Online (Realtime Online)</li><li>Lanjutkan proses otentikasi</li><li>Pembayaran selesai.</li></ol><ul><li>Cara Pembayaran Akun Virtual Bank Niaga Melalui Niaga Mobile</li></ul><ol><li>Pilih &ldquo;Transfer ke Rekening CIMB Niaga Lainnya&rdquo;</li><li>Masukkan No Rekening : 243906181030040</li><li>Contoh No rek tujuan : 243906181030040</li><li>Keterangan No rek tujuan : 2439 adalah 4 digit kode bank, 06181030040 adalah npm/no va</li><li>Masukkan Nominal sesuai tagihan, TANPA BIAYA ADMIN</li><li>Info nominal : nominal yang dimasukkan adalah nominal dari keseluruhan tagihan, tidak bisa memilih salah satu tagihan. Contoh, Riska anjani memiliki tagihan biaya kuliah sebesar Rp7.000 dan tagihan UTS susulan Rp3.000, maka nominal yang dimasukkan adalah Rp10.000</li><li>Disarankan untuk memilih waktu transaksi &ldquo;sekarang&rdquo;</li><li>Klik &ldquo;lanjut&rdquo;</li><li>Pastikan untuk memeriksa tagihan, jika sudah sesuai lalu klik &ldquo;Konfirmasi&rdquo;</li><li>Lanjutkan proses otentikasi</li><li>Pembayaran selesai.</li></ol><p>Catatan:</p><p>Layanan PUPD: Senin s.d Jumat 08.00 &ndash; 16.00</p>')
            ],
          ),
          ExpansionTile(
            title: Text('Etika kuliah tatap muka'),
            children: [
              Html(data: "<p>- Datang tepat waktu sesuai dengan jadwal yang ditentukan.</p><p>- Menggunakan pakaian rapih dan sopan, sesuai dengan ketentuan berikut:</p><ol>	<li>Perempuan: kemeja/ blouse, celana panjang/ rok panjang, sepatu tertutup, jaket (dilarang menggunakan: baju crop, kaos oblong, sandal)</li>	<li>Laki-laki: kemeja, celana Panjang, jaket, sepatu tertutup (dilarang menggunakan: kaos oblong, sandal)</li></ol><p>- Absen tapping di ruang kuliah yang sesuai dengan jadwal.</p><p>- Mengikuti perkuliahan dengan baik</p>")
            ],
          ),
          ExpansionTile(
            title: Text('Tata cara kuliah online'),
            children: [
              Html(data: '<ul><li>Proses enrole</li><li>Login siakad: <a href="https://utama.widyatama.ac.id/">https://utama.widyatama.ac.id/</a></li><li>Join mata kuliah</li><li>Aktifitas kuliah di laman <a href="https://kuliahonline.widyatama.ac.id/">https://kuliahonline.widyatama.ac.id/</a></li><li>Tugas/kuis/forum</li><li>UTS/UAS</li><li>Join virtual meeting</li><li>Presensi mahasiswa</li><li>Auto deteksi mahasiswa kurang aktif</li><li>Presensi mahasiswa secara otomatis</li></ul>')
            ],
          ),
          ExpansionTile(
            title: Text('Pembuatan KTM'),
            children: [
              Html(data: 'Untuk pembuatan KTM dapat datang langsung ke Gedung A lantai 3. Proses pembuatan KTM tidak memakan banyak waktu hanya berkisar 15-20 menit.')
            ],
          ),
          ExpansionTile(
            title: Text('Pengambilan almet '),
            children: [
              Html(data: '<p>Mahasiswa perlu membawa KTM untuk bisa mengambil jas almamater.</p><p>Pengambilan jas almamater bisa dilakukan di Gedung PKM lt. 6 ruang kemahasiswaan.</p><p>Senin &ndash; Jumat: 09.00 &ndash; 16.00</p><p>Sabtu: 09.00 &ndash; 14.00</p><p>Selain jas almamater, mahasiswa juga akan mendapatkan: kaos widyatama dan tumblr minum widyatama.</p>')
            ],
          ),
          ExpansionTile(
            title: Text('Jadwal kuliah'),
            children: [
              Html(data: '<p>Untuk melihat jadwal kuliah bisa cek di laman: <a href="https://utama.widyatama.ac.id/">https://utama.widyatama.ac.id/</a></p>')

            ],
          ),
          ExpansionTile(
            title: Text('Melihat kehadiran'),
            children: [
              Html(data: '<p>Untuk melihat absen kehadiran tapping bisa cek di laman:&nbsp; <a href="http://presensi.widyatama.ac.id/">http://presensi.widyatama.ac.id/</a></p>')
            ],
          ),
          ExpansionTile(
            title: Text('Kontak'),
            children: [
              Html(data: '<p>Akademik:</p><p>JAM LAYANAN AKADEMIK</p><p><strong>Senin - Jumat </strong><br />jam 08.00 - 12.00 &amp; 13.00 - 20.00 WIB</p><p><strong>Sabtu</strong><br />jam 08.00 - 12.00 &amp; 13.00 - 16.00 WIB</p><p>Gedung B Lantai 1</p><p>WA:<br />Reg A : 0855-1707-373<br />Reg B : 0855-1707-575</p><p>Keuangan:</p><p>LAYANAN PUPD</p><p>Senin S.D Jum&#39;at<br />08.00 - 16.00 WIB</p><p>Sabtu<br />08.00 - 12.00 WIB</p><p>Gedung B Lt.1 Ruang B.124</p><p>Reguler A ext: 240<br />No. Hp: 0895353010533</p><p>Reguler B ext: 262<br />No. Hp: 083838693737</p><p>E-learning:</p><p>JAM LAYANAN E-LEARNING</p><p><strong>Senin - Jumat </strong><br />jam 07.00 - 21.00 WIB</p><p><strong>Sabtu</strong><br />jam 07.00 - 18.00 WIB</p><p>Gedung K Lantai 3</p><p>Phone : 022 7275855 Ext : 322<br />WA :<br />Eka Angga L (Admin 1): 082327786991<br />Ale (Admin 2) : 082295000451<br />Paundra (Admin 3) : 085320303065</p><p>Email: elearning@widyatama.ac.id</p>')
            ],
          ),
        ],
      ),
    );
  }
}
