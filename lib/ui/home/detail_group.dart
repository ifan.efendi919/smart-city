// ignore_for_file: prefer_const_constructors

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:qr_flutter/qr_flutter.dart';

import '../../theme/theme_colors.dart';
import '../../widgets/bottom_container_chat.dart';
import '../../widgets/message_design.dart';

class DetailGroupPage extends StatefulWidget {
  final String projectName;
  final String projectId;
  final String createDate;
  const DetailGroupPage({
    super.key,
    required this.projectName,
    required this.projectId,
    required this.createDate,
  });

  @override
  State<DetailGroupPage> createState() => _DetailGroupPageState();
}

class _DetailGroupPageState extends State<DetailGroupPage> {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  late bool isModal = false;
  late int indexTab = 1;

  sendMessage(type) async {
    await FirebaseFirestore.instance
        .collection('course')
        .doc(widget.projectId)
        .collection('chat')
        .add({
      'sender_id': _auth.currentUser!.uid,
      'message': "This is a simple QR code",
      'type': type.toString(),
      'sent_item': DateTime.now(),
    });
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Scaffold(
          backgroundColor: ThemeColors().grey,
          appBar: AppBar(
            scrolledUnderElevation: 0,
            backgroundColor: Colors.white,
            title: Row(
              children: [
                Container(
                  height: 40,
                  width: 40,
                  decoration: BoxDecoration(
                    color: ThemeColors().blue,
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Icon(
                    Icons.group,
                    color: Colors.white,
                  ),
                ),
                SizedBox(
                  width: 10,
                ),
                Expanded(
                  child: Text(
                    widget.projectName,
                    overflow: TextOverflow.ellipsis,
                  ),
                )
              ],
            ),
          ),
          body: Column(
            children: [
              Row(
                mainAxisSize: MainAxisSize.max,
                children: [
                  Expanded(
                    flex: 1,
                    child: GestureDetector(
                      onTap: () {
                        setState(() {
                          indexTab = 1;
                        });
                      },
                      child: Container(
                        alignment: Alignment.center,
                        padding: const EdgeInsets.symmetric(vertical: 15),
                        decoration: BoxDecoration(
                            color: indexTab == 1
                                ? ThemeColors().blue
                                : Color(0xFFB9C1EB)),
                        child: Text(
                          'Course',
                          style: TextStyle(fontSize: 12, color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: GestureDetector(
                      onTap: () {
                        setState(() {
                          indexTab = 2;
                        });
                      },
                      child: Container(
                        alignment: Alignment.center,
                        padding: const EdgeInsets.symmetric(vertical: 15),
                        decoration: BoxDecoration(
                            color: indexTab == 2
                                ? ThemeColors().blue
                                : Color(0xFFB9C1EB)),
                        child: Text(
                          'Chat',
                          style: TextStyle(fontSize: 12, color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              Expanded(
                child: indexTab == 2
                    ? Column(
                        children: [
                          Expanded(
                            child: SingleChildScrollView(
                              reverse: true,
                              physics: BouncingScrollPhysics(),
                              child: Column(
                                children: [
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Expanded(
                                          child: Container(
                                        margin: const EdgeInsets.symmetric(
                                            horizontal: 20),
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 20, vertical: 5),
                                        decoration: BoxDecoration(
                                          color: ThemeColors()
                                              .purpleAccent
                                              .withOpacity(0.5),
                                          borderRadius:
                                              BorderRadius.circular(20),
                                        ),
                                        child: Center(
                                            child: Wrap(
                                          children: [
                                            Text(
                                              '${widget.projectName} was created',
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 16,
                                              ),
                                            ),
                                          ],
                                        )),
                                      ))
                                    ],
                                  ),
                                  Text(
                                    widget.createDate,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 12,
                                    ),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  StreamBuilder(
                                      stream: FirebaseFirestore.instance
                                          .collection('course')
                                          .doc(widget.projectId)
                                          .collection('chat')
                                          .orderBy('sent_item',
                                              descending: true)
                                          .snapshots(),
                                      builder: (context, snapshot) {
                                        if (snapshot.hasData) {
                                          return ListView.builder(
                                            reverse: true,
                                            itemCount:
                                                snapshot.data!.docs.length,
                                            shrinkWrap: true,
                                            physics:
                                                NeverScrollableScrollPhysics(),
                                            itemBuilder: ((context, index) {
                                              var a = DateTime.parse(snapshot
                                                  .data!
                                                  .docs[index]['sent_item']
                                                  .toDate()
                                                  .toString());
                                              var time =
                                                  DateFormat('HH:mm').format(a);

                                              bool isMe =
                                                  snapshot.data!.docs[index]
                                                          ['sender_id'] ==
                                                      _auth.currentUser!.uid;

                                              return MessageDesign(
                                                isMe: isMe,
                                                message: snapshot.data!
                                                    .docs[index]['message'],
                                                time: time,
                                                senderId: snapshot.data!
                                                    .docs[index]['sender_id'],
                                                projectId: widget.projectId,
                                                messageIndex: snapshot
                                                    .data!.docs[index].id,
                                                type: snapshot.data!.docs[index]
                                                    ['type'],
                                              );
                                            }),
                                          );
                                        } else {
                                          return Container();
                                        }
                                      }),
                                ],
                              ),
                            ),
                          ),
                          //Bottom Container
                          BottomContainerChat(
                              projectId: widget.projectId, type: 'text'),
                        ],
                      )
                    : ListView(
                        padding: const EdgeInsets.symmetric(
                            vertical: 20, horizontal: 20),
                        shrinkWrap: true,
                        children: [
                          Text(
                            DateFormat.yMMMMEEEEd('en_US')
                                .format(DateTime.now()),
                            style: TextStyle(
                                fontSize: 12, fontWeight: FontWeight.bold),
                          ),
                          SizedBox(height: 10,),
                          StreamBuilder(
                            stream: FirebaseFirestore.instance
                                .collection('users')
                                .where('role', isEqualTo: 'mahasiswa')
                                .snapshots(),
                            builder: ((context, snapshot) {
                              if (!snapshot.hasData) {
                                return Container();
                              } else {
                                return ListView.builder(
                                  itemCount: snapshot.data!.docs.length,
                                  physics: const ClampingScrollPhysics(),
                                  shrinkWrap: true,
                                  itemBuilder: ((context, index) {
                                    return Container(
                                      padding: const EdgeInsets.all(20),
                                      margin: const EdgeInsets.only(bottom: 10),
                                      decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius: BorderRadius.circular(8.0),
                                        boxShadow: const [
                                          BoxShadow(
                                            color: Colors.black12,
                                            blurRadius: 4,
                                            offset: Offset(4, 8), // Shadow position
                                          ),
                                        ],
                                      ),
                                      child: Row(
                                        children: [
                                          Text('${snapshot.data!.docs[index]['name']}'),
                                        ],
                                      ),
                                    );
                                  }),
                                );
                              }
                            })
                          ),
                          SizedBox(height: 20),
                          Row(
                            mainAxisSize: MainAxisSize.min,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                alignment: Alignment.center,
                                padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 25),
                                decoration: BoxDecoration(
                                  color: ThemeColors().blue,
                                  borderRadius: BorderRadius.circular(8.0)
                                ),
                                child: Text(
                                  'Start Class',
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 12
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
              )
            ],
          ),
        ),
        isModal
            ? Scaffold(
                body: Container(
                  height: Get.height,
                  width: Get.width,
                  decoration:
                      BoxDecoration(color: Colors.black.withOpacity(0.4)),
                  child: Container(
                    padding: const EdgeInsets.all(10),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(8.0)),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text('Show The QR Code To Student Attend This Class'),
                        SizedBox(height: 20),
                        QrImage(
                          data: 'This QR code has an embedded image as well',
                          version: QrVersions.auto,
                          size: 320,
                          gapless: false,
                          embeddedImage:
                              AssetImage('assets/images/my_embedded_image.png'),
                          embeddedImageStyle: QrEmbeddedImageStyle(
                            size: Size(80, 80),
                          ),
                        ),
                        SizedBox(height: 20),
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              isModal = false;
                            });
                          },
                          child: Container(
                            padding: const EdgeInsets.symmetric(
                                vertical: 10, horizontal: 20),
                            decoration: BoxDecoration(
                                color: ThemeColors().blue,
                                borderRadius: BorderRadius.circular(8)),
                            child: Text(
                              'Done',
                              style:
                                  TextStyle(fontSize: 14, color: Colors.white),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              )
            : Container()
      ],
    );
  }
}
