// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors

import 'package:carousel_slider/carousel_slider.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

import 'package:intl/intl.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:smart_city/ui/home/faq.dart';

import '../../theme/theme_colors.dart';
import '../../widgets/custom_widget.dart';
import '../../widgets/widget_home.dart';
import '../project_ui/choose_members.dart';
import 'detail_news.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;
  final FirebaseAuth _auth = FirebaseAuth.instance;

  int? selectedMenu;
  late String? role;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        scrolledUnderElevation: 0,
        toolbarHeight: 15,
      ),
      body: Padding(
        padding: const EdgeInsets.all(15.0),
        child: ListView(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(child: StreamBuilder(
                  stream: FirebaseFirestore.instance
                      .collection('users')
                      .doc(_auth.currentUser!.uid)
                      .snapshots(),
                  builder: ((context, snapshot) {
                    if (!snapshot.hasData) {
                      return Container();
                    } else {
                      role = snapshot.data!.get('role');

                      return Text(
                        'Hello, ${snapshot.data!.get('name')}',
                        style: TextStyle(
                          fontSize: 30,
                          fontWeight: FontWeight.bold,
                          color: ThemeColors().blue
                        ),
                      );
                    }
                  }),
                ),),
                PopupMenuButton<int>(
                  initialValue: selectedMenu,
                  // Callback that sets the selected popup menu item.
                  onSelected: (item) {
                    if(item == 1){
                      Get.to(()=>FAQPAGE());
                    }
                  },
                  icon: Icon(
                    Icons.menu,
                    color: ThemeColors().blue,
                  ),
                  itemBuilder: (BuildContext context) => <PopupMenuEntry<int>>[
                    PopupMenuItem<int>(
                      onTap: (){
                      },
                      value: 1,
                      child: Text('FAQ'),
                    ),
                    PopupMenuItem<int>(
                      onTap: (){
                        FirebaseAuth.instance.signOut();
                      },
                      value: 2,
                      child: Text('Log Out'),
                    ),
                  ],
                )
              ],
            ),
            SizedBox(
              height: 25,
            ),
            //Body layout.
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Hot News',
                  style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.bold,
                      color: ThemeColors().blue
                  ),
                ),
                GestureDetector(
                  onTap: (){},
                  child: Text(
                    "More",
                    style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.bold,
                        color: ThemeColors().blue
                    ),
                  ),
                )
              ],
            ),
            SizedBox(
              height: 15,
            ),
            SizedBox(
              height: 200,
              child: StreamBuilder(
                stream: _firestore.collection('news').snapshots(),
                builder: (context, snapshot){
                  if(snapshot.connectionState == ConnectionState.waiting){
                    return SpinKitFadingCircle(color: ThemeColors().blue,);
                  }else if (snapshot.connectionState == ConnectionState.active
                      || snapshot.connectionState == ConnectionState.done) {
                    if (snapshot.hasError) {
                      return const Text('Error');
                    } else if (snapshot.hasData) {
                      return CarouselSlider(
                        options: CarouselOptions(
                          height: 200
                        ),
                        items: snapshot.data!.docs.map((val) {
                          return GestureDetector(
                            onTap: (){
                              Get.to(DetailNews(id: val.id));
                            },
                            child: Container(
                                width: MediaQuery.of(context).size.width,
                                margin: EdgeInsets.symmetric(horizontal: 5.0),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(8),
                                  image: DecorationImage(
                                    image: NetworkImage(val.get('gambar_url')),
                                    fit: BoxFit.fitHeight
                                  )
                                ),
                                child: Container(
                                  padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                                  decoration: BoxDecoration(
                                    color: Colors.black.withOpacity(0.5),
                                    borderRadius: BorderRadius.circular(8)
                                  ),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text('${val.get('judul')}',
                                        style: TextStyle(
                                          fontSize: 14.0,
                                          color: Colors.white
                                        ),
                                      ),
                                      SizedBox(height: 10.0,),
                                    ],
                                  ),
                                )
                            ),
                          );
                        }).toList()
                      );
                    } else {
                      return const Text('Empty data');
                    }
                  } else {
                    return Text('State: ${snapshot.connectionState}');
                  }
                },
              ),
            ),
            SizedBox(height: 20,),
            Text(
              'Schedule',
              style: TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.bold,
                color: ThemeColors().blue
              ),
            ),
            SizedBox(height: 5,),
            Container(
              padding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8.0),
                border: Border.all(color: ThemeColors().blue)
              ),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        flex: 1,
                        child: Text(
                          "No",
                          style: TextStyle(
                              color: ThemeColors().blue
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      Expanded(
                        flex: 2,
                        child: Text(
                          "Course",
                          style: TextStyle(
                              color: ThemeColors().blue
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      Expanded(
                        flex: 2,
                        child: Text(
                          "Clock",
                          style: TextStyle(
                              color: ThemeColors().blue
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      Expanded(
                        flex: 2,
                        child: Text(
                          "Room",
                          style: TextStyle(
                              color: ThemeColors().blue
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ],
                  ),
                  Divider(color: ThemeColors().blue,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        flex: 1,
                        child: Text(
                          "1",
                          style: TextStyle(
                              color: ThemeColors().blue
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      Expanded(
                        flex: 2,
                        child: Text(
                          "Smart City",
                          style: TextStyle(
                              color: ThemeColors().blue
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      Expanded(
                        flex: 2,
                        child: Text(
                          "18:00",
                          style: TextStyle(
                              color: ThemeColors().blue
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      Expanded(
                        flex: 2,
                        child: Text(
                          "B409",
                          style: TextStyle(
                              color: ThemeColors().blue
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            )
          ],
        ),
      ),
      //New project floating action button.
      // floatingActionButton: FloatingActionButton.extended(
      //   onPressed: () {
      //     Navigator.push(
      //       context,
      //       MaterialPageRoute(
      //         builder: ((context) {
      //           return ChooseMembers();
      //         }),
      //       ),
      //     );
      //   },
      //   backgroundColor: ThemeColors().blue,
      //   label: Text(
      //     'New project',
      //     style: TextStyle(
      //       fontSize: 16,
      //       color: Colors.white,
      //     ),
      //   ),
      // ),
    );
  }
}

enum SampleItem { itemOne, itemTwo, }
